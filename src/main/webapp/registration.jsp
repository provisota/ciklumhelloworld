<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=utf-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <script src="resources/js/jquery-2.1.4.min.js"></script>
    <script src="resources/js/jquery.pstrength-min.1.2.js"></script>
    <script>
        $(function () {
            $('#password').pstrength();
        });
    </script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <title>Registration</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="<c:url value="resources/css/bootstrap.min.css" />"
          rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="CiklumHelloWorld.css">
</head>
<style>
    form {
        width: 500px;
        margin-top: 100px !important;
    }

    fieldset {
        border: 1px solid lightgray;
        border-radius: 4px
    }
</style>
<body>
<div align="center">
    <h2>Registration
        <a title="home page" href="CiklumHelloWorld.html">CiklumHelloWorld</a>
    </h2>

    <p></p>
    <c:url var="regUrl" value="register.do"/>
    <form action="${regUrl}" method="post">
        <p style="color: green; font-size: medium">${requestScope.success_message}</p>
        <fieldset>
            <p></p>
            <table>
                <tr>
                    <!-- Username field -->
                    <th><label for="username">Username</label></th>
                    <td><input style="margin-bottom: 5px"
                               title="could contain only letters and numbers
                               (3 - 12 characters)"
                               class="form-control" id="username"
                               name="username" placeholder="input username"
                               type="text" value="${requestScope.username}"/>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <p style="color: red; margin: auto auto 5px">
                            ${requestScope.error_username}
                        </p>
                    </td>
                </tr>
                <tr>
                    <!-- Email field -->
                    <th><label for="email">E-mail</label></th>
                    <td><input style="margin-bottom: 5px"
                               title="input existing e-mail for registration confirmation"
                               class="form-control" id="email"
                               name="email" placeholder="input e-mail"
                               type="email" value="${requestScope.email}"/>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <p style="color: red; margin: auto auto 5px">
                            ${requestScope.error_email}
                        </p>
                    </td>
                </tr>
                <tr>
                    <!-- Password field -->
                    <th><label for="password">Password</label></th>
                    <td>
                        <input title="have to be Strong (minimum 6 characters) use letters,
                        digits and special characters."
                               class="form-control" id="password"
                               name="password" placeholder="input password"
                               type="password" value="${requestScope.password}"/>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <p style="color: red; margin: auto auto 5px">
                            ${requestScope.error_password}
                        </p>
                    </td>
                </tr>
                <tr>
                    <!-- Password confirmation -->
                    <th>
                        <label style="margin-right: 10px" for="password">
                            Confirm pasword
                        </label>
                    </th>
                    <td><input style="margin-bottom: 5px"
                               title="must match with password"
                               class="form-control" id="confirm_password"
                               name="confirm_password" placeholder="confirm pasword"
                               type="password" value="${requestScope.confirm_password}"/>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <p style="color: red; margin: auto auto 5px">
                            ${requestScope.error_confirm_password}
                        </p>
                    </td>
                </tr>
            </table>
            <div class="g-recaptcha"
                 data-sitekey="6Ld-ogATAAAAAOzGjMIoTkptzJ_NomGP8HfApVfk"></div>
            <p></p>
            <p style="color: red; font-size: medium">${requestScope.error_message}</p>
            <button style="width: 200px; margin: auto auto 5px;"
                    class="btn btn-success btn-block" name="commit"
                    type="submit">Registration
            </button>
        </fieldset>
    </form>
    <script>
        document.getElementById('username').focus();
    </script>
</div>
</body>
</html>
