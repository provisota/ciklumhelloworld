<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=utf-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Authorization</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="<c:url value="resources/css/bootstrap.min.css" />"
          rel="stylesheet">
</head>
<style>
    form {
        width: 500px;
        margin-top: 100px !important;
    }

    fieldset {
        border: 1px solid lightgray;
        border-radius: 4px
    }
</style>
<body>
<div align="center">
    <h2>Welcome to
        <a title="home page" href="CiklumHelloWorld.html">CiklumHelloWorld</a>
    </h2>

    <p></p>
    <!-- Path to authentification filter -->
    <c:url var="authUrl" value="/static/j_spring_security_check"/>
    <form method="post" action="${authUrl}">
        <fieldset>
            <p style="color: red">
                <c:if test="${param.error != null}">
                    <c:out value="Wrong login or/and password"/>
                </c:if>
            </p>

            <p style="color: green">${requestScope.success_message}</p>
            <table>
                <tr>
                    <th>
                        <label style="margin-right: 10px" for="username_or_email">
                            Username
                        </label>
                    </th>
                    <td><input style="margin-bottom: 5px"
                               class="form-control" id="username_or_email"
                               name="j_username" value="${requestScope.username}"
                               placeholder="input username" type="text"/>
                        <!-- Username field -->
                    </td>
                </tr>
                <tr>
                    <th>
                        <label style="margin-right: 10px" for="password">
                            Password
                        </label>
                    </th>
                    <td><input class="form-control" id="password"
                               name="j_password" value="${requestScope.password}"
                               placeholder="input password" type="password"/>
                        <!-- Password field -->
                        <small><a href="resend.jsp">Forgot password?</a></small>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td><input id="remember_me"
                               name="_spring_security_remember_me"
                               type="checkbox"/> <!-- "remember me" flag-->
                        <label for="remember_me"
                               class="inline">remember me</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button style="width: 200px; margin: auto auto 5px;"
                                class="btn btn-primary btn-block" name="commit"
                                type="submit">Login
                        </button>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a style="width: 200px; margin: auto auto 5px;"
                           class="btn btn-success btn-block"
                           href="registration.jsp">Register</a>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
    <script type="text/javascript">
        document.getElementById('username_or_email').focus();
    </script>
</div>
</body>
</html>