package com.ciklum.helloworld.server.registration;

import com.ciklum.helloworld.shared.entity.user.UserEntity;
import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * @author Alterovych Ilya
 */
public class UserPasswordResender extends Thread{

    private static final Logger logger = Logger.getLogger(UserPasswordResender.class);
    private UserEntity userEntity;
    private String baseUrl;

    public UserPasswordResender(UserEntity userEntity, String baseUrl) {
        this.userEntity = userEntity;
        this.baseUrl = baseUrl;
        start();
    }

    @Override
    public void run() {

        String to = userEntity.getEmail();
        String from = "provisota@gmail.com";
        String confirmationLink = baseUrl + "confirm.do?confirm_id=" +
                userEntity.getConfirmId() + "&is_resend=true";
        final String username = "mailsender2015@gmail.com";
        final String password = "mmmm1978";

        Properties properties = new Properties();
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.user", username);
        properties.put("mail.smtp.password", password);
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject("Password recovery.");
            message.setContent(
                    "<h1>Password recovery.</h1>" +
                            "<a style=\"border: 1px solid lightblue; " +
                            "border-radius: 4px; padding: 7px; margin-bottom: 10px; " +
                            "background-color: whitesmoke; font-size: large;\" href=" +
                            "\"" + confirmationLink + "\">" +
                            "Recover password</a><br/><br/>" +
                            "or copy this link:<br/> " + confirmationLink +
                            " <br/>and paste it to browser address bar.",
                    "text/html; charset=utf-8");
            Transport.send(message);
            logger.info("Send message successfully.... UUID = " +
                    userEntity.getConfirmId());
        } catch (MessagingException mex) {
            mex.printStackTrace();
            logger.error("Error message sending: " + mex);
        }
    }
}
