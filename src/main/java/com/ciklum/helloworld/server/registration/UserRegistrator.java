package com.ciklum.helloworld.server.registration;

import com.ciklum.helloworld.shared.entity.user.UserEntity;
import com.ciklum.helloworld.server.services.UserService;
import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import java.util.UUID;

/**
 * @author Alterovych Ilya
 */
public class UserRegistrator extends Thread {

    private static final Logger logger = Logger.getLogger(UserRegistrator.class);
    private UserEntity userEntity;
    private UserService userService;
    private String baseUrl;

    public UserRegistrator(UserEntity userEntity, UserService userService, String baseUrl) {
        this.userEntity = userEntity;
        this.userService = userService;
        this.baseUrl = baseUrl;
        start();
    }

    @Override
    public void run() {
        String uuid = UUID.randomUUID().toString();
        userEntity.setConfirmId(uuid);
        userService.add(userEntity);

        String to = userEntity.getEmail();
        String from = "provisota@gmail.com";
        String confirmationLink = baseUrl + "confirm.do?confirm_id=" +
                uuid + "&is_resend=false";
        final String username = "mailsender2015@gmail.com";
        final String password = "mmmm1978";

        Properties properties = new Properties();
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.user", username);
        properties.put("mail.smtp.password", password);
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject("Please confirm your registration.");
            message.setContent(
                    "<h1>Please confirm your registration.</h1>" +
                            "<a style=\"border: 1px solid lightblue; " +
                            "border-radius: 4px; padding: 7px; margin-bottom: 10px; " +
                            "background-color: whitesmoke; font-size: large;\" href=" +
                            "\"" + confirmationLink + "\">" +
                            "Confirm your registration</a><br/><br/>" +
                            "or copy this link:<br/> " + confirmationLink +
                            " <br/>and paste it to browser address bar.",
                    "text/html; charset=utf-8");
            Transport.send(message);
            logger.info("Sent message successfully.... UUID = " + uuid);
        } catch (MessagingException mex) {
            mex.printStackTrace();
            logger.error("Error message sending: " + mex);
        }
    }
}
