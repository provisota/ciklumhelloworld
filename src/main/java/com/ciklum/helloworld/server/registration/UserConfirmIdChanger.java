package com.ciklum.helloworld.server.registration;

import com.ciklum.helloworld.shared.entity.user.UserEntity;
import com.ciklum.helloworld.server.services.UserService;

import java.util.UUID;


/**
 * @author Alterovych Ilya
 * Set new confirmId for given to constructor user,
 * and save it to data base
 */
public class UserConfirmIdChanger extends Thread {
    private UserEntity userEntity;
    private UserService userService;

    public UserConfirmIdChanger(UserEntity userEntity, UserService userService) {
        this.userEntity = userEntity;
        this.userService = userService;
        start();
    }

    @Override
    public void run() {
        String uuid = UUID.randomUUID().toString();
        userEntity.setConfirmId(uuid);
        userService.add(userEntity);
    }
}
