package com.ciklum.helloworld.server.dao;

import com.ciklum.helloworld.shared.entity.user.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Alterovych Ilya
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {
    UserEntity findByUsername(String username);
    UserEntity findByEmail(String email);
    UserEntity findByConfirmId(String confirmId);
    List<UserEntity> findByIsConfirmed(Boolean isConfirmed);
    void deleteByUsername(String username);
}
