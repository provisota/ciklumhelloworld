package com.ciklum.helloworld.server.services;

import com.ciklum.helloworld.shared.entity.user.UserEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * @author Alterovych Ilya
 */
@Service
public interface UserService extends UserDetailsService {
    List<UserEntity> getAll();

    UserEntity edit(UserEntity userEntity);

    UserEntity add(UserEntity userEntity);

    List<UserEntity> addBatch(Set<UserEntity> userEntities);

    void deleteBatch(Set<UserEntity> userEntities);

    UserEntity findByUsername(String username);

    UserEntity findByEmail(String email);

    UserEntity findByConfirmId(String confirmId);

    void addTestUsers();

    UserEntity getLoggedInUser();

    void deleteByUsername(String username);
}
