package com.ciklum.helloworld.server.services.impl;

import com.ciklum.helloworld.server.dao.UserRepository;
import com.ciklum.helloworld.server.services.UserService;
import com.ciklum.helloworld.shared.entity.user.UserEntity;
import com.ciklum.helloworld.shared.enums.Role;
import com.ciklum.helloworld.shared.utils.Utils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * @author Alterovych Ilya
 */
@Transactional
@Service("userService")
public class UserServiceImpl implements UserService {
    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);
    private boolean testUsersAdded;

    @Autowired
    UserRepository userRepository;

    @Override
    public List<UserEntity> getAll() {
        return userRepository.findByIsConfirmed(true);
    }

    @Override
    public UserEntity edit(UserEntity userEntity) {
        return add(userEntity);
    }

    /**
     * Save new User in DB
     * @param userEntity User for save
     * @return saved User
     */
    @Override
    public UserEntity add(UserEntity userEntity) {
        userEntity.setPassword(Utils.encodePassword(userEntity.getPassword()));
        UserEntity savedUserEntity = userRepository.saveAndFlush(userEntity);
        if (logger.isInfoEnabled()) {
            logger.info("Saved user: " + savedUserEntity);
        }
        return savedUserEntity;
    }

    @Override
    public List<UserEntity> addBatch(Set<UserEntity> userEntities) {
        for (UserEntity userEntity : userEntities) {
            userEntity.setPassword(Utils.encodePassword(userEntity.getPassword()));
        }
        return userRepository.save(userEntities);
    }

    @Override
    public void deleteBatch(Set<UserEntity> userEntities) {
        userRepository.deleteInBatch(userEntities);
    }

    @Override
    public UserEntity findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public UserEntity findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public UserEntity findByConfirmId(String confirmId) {
        return userRepository.findByConfirmId(confirmId);
    }

    /**
     * Add test users with fake E-mail and fake confirmID
     */
    @Override
    public void addTestUsers() {
        if (!checkNameExist("user") && !checkEmailExist("user@mail.com")) {
            add(new UserEntity("user", "user@mail.com", "user", Role.ROLE_USER, true,
                    "8ed68572-f8eb-4126-9ec3-aca8b3e22d2b"));
        }
        if (!checkNameExist("admin") && !checkEmailExist("admin@mail.com")) {
            add(new UserEntity("admin", "admin@mail.com", "admin",
                    Role.ROLE_ADMIN, true, "ab575892-c92f-4838-ab0e-c0517c54ddc9"));
        }
        if (!checkNameExist("anonymousUser") &&
                !checkEmailExist("anonymousUser@mail.com")) {
            add(new UserEntity("anonymousUser", "anonymousUser@mail.com", "admin",
                    Role.ROLE_ADMIN, true, "cfc5d8fb-7c99-42f5-8295-fc5ec1f9fcbd"));
        }
    }

    @Override
    public UserEntity getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null) {
            logger.warn("Not logged in");
            return null;
        } else {
            return findByUsername((String) authentication.getPrincipal());
        }
    }

    private boolean checkEmailExist(String email) {
        boolean isExist = findByEmail(email) != null;
        if (isExist) {
            logger.warn("E-mail '" + email + "' is already registered.");
        }
        return isExist;
    }

    private boolean checkNameExist(String username) {
        boolean isExist = findByUsername(username) != null;
        if (isExist) {
            logger.warn("Login '" + username + "' is already registered.");
        }
        return isExist;
    }

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException, DataAccessException {
        //Add test user & admin records (remove for production)
        /*if (!testUsersAdded) {
            addTestUsers();
            testUsersAdded = true;
        }*/
        UserEntity userEntity = findByUsername(username);
        if (userEntity == null) {
            throw new UsernameNotFoundException("user not found");
        }
        return Utils.buildUserFromUserEntity(userEntity);
    }

    @Override
    public void deleteByUsername(String username) {
        userRepository.deleteByUsername(username);
    }
}
