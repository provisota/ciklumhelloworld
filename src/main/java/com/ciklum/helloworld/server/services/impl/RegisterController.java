package com.ciklum.helloworld.server.services.impl;

import com.ciklum.helloworld.server.registration.UserConfirmIdChanger;
import com.ciklum.helloworld.server.registration.UserPasswordResender;
import com.ciklum.helloworld.server.registration.UserRegistrator;
import com.ciklum.helloworld.server.services.UserService;
import com.ciklum.helloworld.shared.entity.user.UserEntity;
import com.ciklum.helloworld.shared.enums.Role;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

import static com.ciklum.helloworld.shared.utils.Utils.*;

/**
 * @author Alterovych Ilya
 */
@Controller
public class RegisterController {
    private static final Logger logger = Logger.getLogger(RegisterController.class);

    @Autowired
    UserService userService;

    @RequestMapping(value = "/register.do", method = RequestMethod.POST)
    public String addNewUser(@RequestParam("username") final String username,
                             @RequestParam("email") final String email,
                             @RequestParam("password") final String password,
                             @RequestParam("confirm_password") final String confirmPassword,
                             @RequestParam("g-recaptcha-response") String recaptcha,
                             Model model) {
        String requestUrl = "https://www.google.com/recaptcha/api/siteverify?" +
                "secret=6Ld-ogATAAAAAKC11Ld3w80Eeu4dLSeJAmuXbEL1&response=" + recaptcha;
        model.addAttribute("username", username);
        model.addAttribute("email", email);
        model.addAttribute("password", password);
        model.addAttribute("confirm_password", confirmPassword);

        if (!isValidUserName(model, username) | !isValidEmail(model, email)
                | !isValidPassword(model, password)
                | !isValidConfirmPassword(model, password, confirmPassword)
                | !checkRecaptcha(model, requestUrl)) {
            return "registration";
        }

        if (isUserNameExist(model, username) | isEmailExist(model, email)) {
            return "registration";
        }

        resetForm(model);

        new UserRegistrator(new UserEntity(username, email,
                password, Role.ROLE_USER, false, "fakeConfirmID"),
                userService, getBaseUrl());

        model.addAttribute("success_message",
                "For 1 - 3 minutes you will receive confirmation email.");
        return "registration";
    }

    @RequestMapping(value = "/resend.do", method = RequestMethod.POST)
    public String resendPassword(@RequestParam("email") String email,
                                 @RequestParam("g-recaptcha-response") String recaptcha,
                                 Model model) {
        String requestUrl = "https://www.google.com/recaptcha/api/siteverify?" +
                "secret=6Ld-ogATAAAAAKC11Ld3w80Eeu4dLSeJAmuXbEL1&response=" + recaptcha;
        model.addAttribute("email", email);
        if (!isValidEmail(model, email) || !checkRecaptcha(model, requestUrl)) {
            return "resend";
        }

        model.addAttribute("success_message",
                "Confirmation email has been sent to your address");
        UserEntity confirmedUser = userService.findByEmail(email);
        if (confirmedUser == null) {
            return "login";
        }
        new UserPasswordResender(confirmedUser, getBaseUrl());
        return "login";
    }

    @RequestMapping(value = "/confirm.do", method = RequestMethod.GET)
    public String confirmUser(@RequestParam("confirm_id") String confirmId,
                              @RequestParam("is_resend") boolean isResendConfirm,
                              Model model) {
        resetForm(model);
        UserEntity confirmedUser = userService.findByConfirmId(confirmId);
        if (confirmedUser == null) {
            model.addAttribute("error_message",
                    "This account does not exist, please register.");
            return "registration";
        }

        model.addAttribute("confirm_id", confirmId);
        if (isResendConfirm) {
            return "change_password";
        }

        confirmedUser.setConfirmed(true);
        userService.edit(confirmedUser);
        new UserConfirmIdChanger(confirmedUser, userService);
        model.addAttribute("success_message",
                "Your account successfully confirmed, now you can login.");
        return "login";
    }

    @RequestMapping(value = "change_password", method = RequestMethod.POST)
    public String changePassword(@RequestParam("confirm_id") String confirmId,
                                 @RequestParam("password") final String password,
                                 @RequestParam("confirm_password") final
                                 String confirmPassword,
                                 Model model) {
        model.addAttribute("confirm_id", confirmId);
        if (!isValidPassword(model, password) |
                !isValidConfirmPassword(model, password, confirmPassword)) {
            return "change_password";
        }

        UserEntity confirmedUser = userService.findByConfirmId(confirmId);
        if (confirmedUser == null) {
            model.addAttribute("error_message",
                    "This account does not exist, please register.");
            return "registration";
        }
        confirmedUser.setPassword(password);
        confirmedUser.setConfirmed(true);
        userService.edit(confirmedUser);
        model.addAttribute("success_message",
                "Your password successfully changed, now you can login.");
        new UserConfirmIdChanger(confirmedUser, userService);
        return "login";
    }

    private String getBaseUrl() {
        ServletRequestAttributes servletRequestAttributes = ((ServletRequestAttributes)
                RequestContextHolder.currentRequestAttributes());
        HttpServletRequest request = servletRequestAttributes.getRequest();

        if ((request.getServerPort() == 80) ||
                (request.getServerPort() == 443)) {
            return request.getScheme() + "://" + request.getServerName() +
                    request.getContextPath() + "/";
        } else {
            return request.getScheme() + "://" + request.getServerName() + ":" +
                    request.getServerPort() + request.getContextPath() + "/";
        }
    }

    private boolean isEmailExist(Model model, String email) {
        if (userService.findByEmail(email) == null) {
            return false;
        } else {
            model.addAttribute("error_email", "this e-mail already registered");
            return true;
        }
    }

    private boolean isUserNameExist(Model model, String username) {
        if (userService.findByUsername(username) == null) {
            return false;
        } else {
            model.addAttribute("error_username", "this username already registered");
            return true;
        }
    }
}
