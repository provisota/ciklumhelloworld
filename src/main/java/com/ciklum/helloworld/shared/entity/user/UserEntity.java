package com.ciklum.helloworld.shared.entity.user;

import com.ciklum.helloworld.shared.enums.Role;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Alterovych Ilya
 */
@Entity
@Table(name = "user")
public class UserEntity implements Serializable{
    private static final long serialVersionUID = 2289139131597244320L;

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.ROLE_USER; //set Role.ROLE_USER as default value

    @Column(name = "is_confirmed", nullable = false)
    private boolean isConfirmed = false; //set false as default value

    @Column(name = "confirm_id", nullable = false)
    private String confirmId;

    public UserEntity() {
    }

    public UserEntity(String username, String email, String password,
                      Role role, boolean isConfirmed, String confirmId) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = role;
        this.isConfirmed = isConfirmed;
        this.confirmId = confirmId;
    }

    //setters & getters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean isConfirmed) {
        this.isConfirmed = isConfirmed;
    }

    public String getConfirmId() {
        return confirmId;
    }

    public void setConfirmId(String confirmId) {
        this.confirmId = confirmId;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + getId() +
                ", username='" + getUsername() + '\'' +
                ", email='" + getEmail() + '\'' +
                ", password='" + getPassword() + '\'' +
                ", role=" + getRole() +
                ", isConfirmed=" + isConfirmed() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserEntity that = (UserEntity) o;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) {
            return false;
        }
        if (!getUsername().equals(that.getUsername())) {
            return false;
        }
        return getEmail().equals(that.getEmail());

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + getUsername().hashCode();
        result = 31 * result + getEmail().hashCode();
        return result;
    }
}
