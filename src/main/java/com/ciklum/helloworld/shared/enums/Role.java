package com.ciklum.helloworld.shared.enums;

/**
 * @author Alterovych Ilya
 */
public enum  Role {
    ROLE_USER, ROLE_ADMIN
}
