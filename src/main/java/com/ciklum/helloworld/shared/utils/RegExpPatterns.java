package com.ciklum.helloworld.shared.utils;

public class RegExpPatterns {
    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+" +
            "(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    //could contain only letters and numbers (3 - 12 characters)
    public static final String LOGIN_PATTERN = "([a-zA-Z0-9]{3,12})$";
    //must contain at least one letter and one number (6 - 15 characters)
    public static final String PASSWORD_PATTERN = "^(?=.*?[A-Za-z])(?=.*?[0-9]).{6,15}$";

}
