package com.ciklum.helloworld.shared.utils;

import com.ciklum.helloworld.shared.entity.user.UserEntity;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Alterovych Ilya
 */
public final class Utils {
    private final static Logger logger = Logger.getLogger(Utils.class);
    private static Pattern pattern;
    private static Matcher matcher;

    private Utils () {
    }

    public static boolean isValidConfirmPassword(Model model, String password,
                                           String confirmPassword) {
        if (confirmPassword != null && confirmPassword.equals(password)) {
            return true;
        } else {
            model.addAttribute("error_confirm_password", "passwords do not match");
            return false;
        }
    }

    public static boolean isValidPassword(Model model, String password) {
        if (password == null || password.isEmpty()) {
            model.addAttribute("error_password", "invalid password");
            return false;
        }

        if (password.length() > 15) {
            model.addAttribute("error_password", "too long password (max 15 characters)");
            return false;
        }

        if (isPasswordStrong(password)) {
            return true;
        } else {
            model.addAttribute("error_password", "too weak password");
            return false;
        }
    }

    public static boolean isValidEmail(Model model, String email) {
        if (email == null || email.isEmpty()) {
            model.addAttribute("error_email", "invalid email address");
            return false;
        }
        pattern = Pattern.compile(RegExpPatterns.EMAIL_PATTERN);
        matcher = pattern.matcher(email);

        if (matcher.matches()) {
            return true;
        } else {
            model.addAttribute("error_email", "invalid email address");
            return false;
        }
    }

    public static boolean isValidUserName(Model model, String username) {
        if (username == null || username.isEmpty()) {
            model.addAttribute("error_username", "invalid account username");
            return false;
        }
        pattern = Pattern.compile(RegExpPatterns.LOGIN_PATTERN);
        matcher = pattern.matcher(username);

        if (matcher.matches()) {
            return true;
        } else {
            model.addAttribute("error_username", "invalid account username");
            return false;
        }
    }

    public static void resetForm(Model model) {
        model.addAttribute("username", "");
        model.addAttribute("email", "");
        model.addAttribute("password", "");
        model.addAttribute("confirm_password", "");
    }

    public static boolean checkRecaptcha(Model model, String requestUrl) {
        BufferedReader in = null;
        boolean response = false;
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();
            String responseMessage = connection.getResponseMessage();

            logger.debug("Sending 'GET' request to recaptcha URL : " + url);
            logger.debug("Response code : " + responseCode + " " + responseMessage);

            in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));

            HashMap recaptchaResponse = new ObjectMapper().readValue(in, HashMap.class);
            response = (boolean) recaptchaResponse.get("success");
            logger.debug("recaptcha response: " + response);
        } catch (IOException e) {
            logger.error("Something wrong with recaptcha");
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!response){
            model.addAttribute("error_message",
                    "Please confirm that you are not a robot.");
        }
        return response;
    }

    public static boolean isPasswordStrong (final String password) {
        boolean hasLetter = false;
        int strength = 0;

        String LETTER = "([a-zA-Z])";
        int MIN_CHAR = 6;
        String [] badWords = {"password", "sex", "god", "123456", "123",
                "liverpool", "letmein", "qwerty", "monkey"};
        int STRONG_SCORE = 30;
        String LOWER_CASE = "[a-z]";
        String UPPER_CASE = "[A-Z]";
        String DIGIT = "\\d+";
        String THREE_DIGIT = "(.*[0-9].*[0-9].*[0-9])";
        String SPECIAL = ".[!,@,#,$,%,^,&,*,?,_,~]";
        String TWO_SPECIAL
                = "(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])";
        String COMBO1 = "([a-z].*[A-Z])|([A-Z].*[a-z])";
        String COMBO2 = "([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])" +
                "|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9])";

        if (password.length() < MIN_CHAR) {
            strength -= 100;
        } else {
            if (password.length() >= MIN_CHAR && password.length() <= (MIN_CHAR + 2)) {
                strength += 6;
            } else {
                if (password.length() >= (MIN_CHAR + 3)
                        && password.length() <= (MIN_CHAR + 4)) {
                    strength += 12;
                } else {
                    if (password.length() >= (MIN_CHAR + 5)) {
                        strength += 18;
                    }
                }
            }

        }
        pattern = Pattern.compile(LETTER);
        matcher = pattern.matcher(password);
        if (matcher.find()) {
            hasLetter = true;
        }
        pattern = Pattern.compile(LOWER_CASE);
        matcher = pattern.matcher(password);
        if (matcher.find()) {
            strength += 1;
        }
        pattern = Pattern.compile(UPPER_CASE);
        matcher = pattern.matcher(password);
        if (matcher.find()) {
            strength += 5;
        }
        pattern = Pattern.compile(DIGIT);
        matcher = pattern.matcher(password);
        if (matcher.find()) {
            strength += 5;
            if (hasLetter) {
                strength += 3;
            }
        }
        pattern = Pattern.compile(THREE_DIGIT);
        matcher = pattern.matcher(password);
        if (matcher.find()) {
            strength += 7;
        }
        pattern = Pattern.compile(SPECIAL);
        matcher = pattern.matcher(password);
        if (matcher.find()) {
            strength += 5;
        }
        pattern = Pattern.compile(TWO_SPECIAL);
        matcher = pattern.matcher(password);
        if (matcher.find()) {
            strength += 7;
        }
        pattern = Pattern.compile(COMBO1);
        matcher = pattern.matcher(password);
        if (matcher.find()) {
            strength += 2;
        }
        pattern = Pattern.compile(COMBO2);
        matcher = pattern.matcher(password);
        if (matcher.find()) {
            strength += 3;
        }

        for (String badWord : badWords) {
            if (password.toLowerCase().equals(badWord)) {
                strength = -200;
            }
        }

        return strength > STRONG_SCORE;
    }

    /*public static void main(String[] args) {
        System.out.println(encodePassword("strongP@$$word1"));
    }*/

    public static String encodePassword(final String password) {
        if (password.length() > 15) {
            return password;
        }
        String hashedPassword = null;
        int i = 0;
        while (i < 10) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            hashedPassword = passwordEncoder.encode(password);
            i++;
        }
        return hashedPassword;
    }

    public static User buildUserFromUserEntity(UserEntity userEntity) {
        String username = userEntity.getUsername();
        String password = userEntity.getPassword();
        boolean enabled = userEntity.isConfirmed();
        boolean accountNonExpired = userEntity.isConfirmed();
        boolean credentialsNonExpired = userEntity.isConfirmed();
        boolean accountNonLocked = userEntity.isConfirmed();

        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(userEntity.getRole().name()));

        return new User(username, password, enabled,
                accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }
}
