package com.ciklum.helloworld.shared.utils;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.ExtendedModelMap;

import static com.ciklum.helloworld.shared.utils.Utils.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Alterovych Ilya
 */
public class UtilsTest {
    private static final String TOO_LONG_STRING = "1234567890123456";
    private static final String TOO_SHORT_STRING = "12";
    private static final String WEAK_PASSWORD = "aaaa1111";
    private static final String STRONG_PASSWORD = "strongP@$$word1";
    private static final String EMPTY_STRING = "";
    private static final String NULL_STRING = null;
    private static final String VALID_EMAIL = "user@mail.ru";
    private static final String INVALID_EMAIL = "user@mailru";
    private static final String VALID_LOGIN = "userNick1978";
    private static final String INVALID_LOGIN = "user@Nick";
    private static final String BCRYPT = "$2a$10$HOpsD8VhDarjkrB3VyP1ne4B1z6" +
            "9oxuWVf7IgEw5ADY6DRpNbUrfq";
    private static final ExtendedModelMap mockModel = new ExtendedModelMap();

    @Test
    public void testIsValidConfirmPasswordForEqualPasswords() throws Exception {
        assertTrue(isValidConfirmPassword(mockModel, STRONG_PASSWORD,
                STRONG_PASSWORD));
    }

    @Test
    public void testIsValidConfirmPasswordForNotEqualPasswords() throws Exception {
        assertFalse(isValidConfirmPassword(mockModel, STRONG_PASSWORD,
                WEAK_PASSWORD));
    }

    @Test
    public void testIsValidPasswordForStrong() throws Exception {
        assertTrue(isValidPassword(mockModel, STRONG_PASSWORD));
    }

    @Test
    public void testIsValidPasswordForEmpty() throws Exception {
        assertFalse(isValidPassword(mockModel, EMPTY_STRING));
    }

    @Test
    public void testIsValidPasswordForNull() throws Exception {
        assertFalse(isValidPassword(mockModel, NULL_STRING));
    }

    @Test
    public void testIsValidPasswordForTooLong() throws Exception {
        assertFalse(isValidPassword(mockModel, TOO_LONG_STRING));
    }

    @Test
    public void testIsValidPasswordForTooShort() throws Exception {
        assertFalse(isValidPassword(mockModel, TOO_SHORT_STRING));
    }

    @Test
    public void testIsValidEmailForValid() throws Exception {
        assertTrue(isValidEmail(mockModel, VALID_EMAIL));
    }

    @Test
    public void testIsValidEmailForInvalid() throws Exception {
        assertFalse(isValidEmail(mockModel, INVALID_EMAIL));
    }

    @Test
    public void testIsValidEmailForEmpty() throws Exception {
        assertFalse(isValidEmail(mockModel, EMPTY_STRING));
    }

    @Test
    public void testIsValidEmailForNull() throws Exception {
        assertFalse(isValidEmail(mockModel, NULL_STRING));
    }

    @Test
    public void testIsValidUserNameForValid() throws Exception {
        assertTrue(isValidUserName(mockModel, VALID_LOGIN));
    }

    @Test
    public void testIsValidUserNameForInvalid() throws Exception {
        assertFalse(isValidUserName(mockModel, INVALID_LOGIN));
    }

    @Test
    public void testIsValidUserNameForNull() throws Exception {
        assertFalse(isValidUserName(mockModel, NULL_STRING));
    }

    @Test
    public void testIsValidUserNameForEmpty() throws Exception {
        assertFalse(isValidUserName(mockModel, EMPTY_STRING));
    }

    @Test
    public void testIsValidUserNameForTooShort() throws Exception {
        assertFalse(isValidUserName(mockModel, TOO_SHORT_STRING));
    }

    @Test
    public void testIsValidUserNameForTooLong() throws Exception {
        assertFalse(isValidUserName(mockModel, TOO_LONG_STRING));
    }

    @Test
    public void testIsPasswordStrongForWeak() throws Exception {
        assertFalse(isPasswordStrong(WEAK_PASSWORD));
    }

    @Test
    public void testIsPasswordStrongForStrong() throws Exception {
        assertTrue(isPasswordStrong(STRONG_PASSWORD));
    }

    @Test
    public void testEncodePassword() throws Exception {
        assertTrue(new BCryptPasswordEncoder().matches(STRONG_PASSWORD,
                encodePassword(STRONG_PASSWORD)));
    }

    @Test
    public void testEncodePasswordForAlreadyEncoded() throws Exception {
        assertEquals(BCRYPT, encodePassword(BCRYPT));
    }
}