package com.ciklum.helloworld.server.services.impl;

import com.ciklum.helloworld.server.services.UserService;
import com.ciklum.helloworld.shared.entity.user.UserEntity;
import com.ciklum.helloworld.shared.enums.Role;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.assertEquals;

/**
 * @author Alterovych Ilya
 */
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@Transactional
public class UserServiceImplTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = Logger.getLogger(UserServiceImplTest.class);
    @Autowired
    UserService userService;

    private static Set<UserEntity> testUsers = new HashSet<>(Arrays.asList(new UserEntity[]{
            new UserEntity("user", "user@mail.com", "password", Role.ROLE_USER, true,
                    "userConfirmId"), new UserEntity("admin", "admin@mail.com",
            "password", Role.ROLE_USER, true, "adminConfirmId"), new UserEntity("anonymous",
            "anonymous@mail.com", "password", Role.ROLE_USER, true, "anonymousConfirmId")}));

    @BeforeClass
    public void setUp() throws Exception {
        userService.addBatch(testUsers);
    }

    @AfterClass
    public void tearDown() throws Exception {
        userService.deleteBatch(testUsers);
    }

    @DataProvider(name = "testData")
    public Object[][] testData() {
        return new Object[][]{
                {"user", "user@mail.com", "userConfirmId"},
                {"admin", "admin@mail.com", "adminConfirmId"},
                {"anonymous", "anonymous@mail.com", "anonymousConfirmId"}
        };
    }

    @Test(dataProvider = "testData")
    public void testFindByUsername(String username, String email, String confirmId)
            throws Exception {
        UserEntity userEntity = userService.findByUsername(username);
        assertThat(userEntity).isNotNull();
        assertEquals(userEntity.getUsername(), username);
        assertEquals(userEntity.getEmail(), email);
        assertEquals(userEntity.getConfirmId(), confirmId);
    }

    @Test(dataProvider = "testData")
    public void testFindByEmail(String username, String email, String confirmId)
            throws Exception {
        UserEntity userEntity = userService.findByEmail(email);
        assertThat(userEntity).isNotNull();
        assertEquals(userEntity.getUsername(), username);
        assertEquals(userEntity.getEmail(), email);
        assertEquals(userEntity.getConfirmId(), confirmId);
    }

    @Test(dataProvider = "testData")
    public void testFindByConfirmId(String username, String email, String confirmId)
            throws Exception {
        UserEntity userEntity = userService.findByConfirmId(confirmId);
        assertThat(userEntity).isNotNull();
        assertEquals(userEntity.getUsername(), username);
        assertEquals(userEntity.getEmail(), email);
        assertEquals(userEntity.getConfirmId(), confirmId);
    }
}