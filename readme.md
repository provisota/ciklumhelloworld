**Java Assignment for Java Developer in Ciklum**

The purpose of this assignment is to put your Java capabilities to a test with respect to writing web based
applications, write high quality code and produce intuitive user interfaces. Your skills in Spring and
Hibernate are also considered.

**Case**

We want to protect our business critical “Hello World!” web page from non-registered users. Only
registered users are granted access to the page.

**Functional requirements:**

* Anonymous users may register by creating a user account from the login page
* A user account consists of a login name and a password
* The password of an account is required to be strong
* Users that login with a valid login name and password are authenticated
* Only authenticated users may view the “Hello World!” dummy page
* Visual strength indicator while setting up the password using Ajax
The algorithm is so complex and business critical that it must be performed server side.

**Non-functional requirements:**

* Provide a readme file
Make application compile and run with up to three command line commands. Hint words: Maven2, Jetty, H2, and HSQL.
* Provide a description of the project structure and the decisions made
* Store the account settings in a SQL database using Hibernate
* Store passwords securely
* Multiple browser support
* Use valid XHTML and CSS
* Provide unit tests using JUnit
* Use the principles of test driven development
* Provide the solution as a Maven project
* Intuitive user interface